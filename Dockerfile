FROM rust

# add latest nightly toolchain (required to run cargo udeps)
RUN rustup toolchain install nightly --profile=minimal

# add 1.65 toolchain
RUN rustup toolchain install 1.65 --profile=minimal

# install clippy
RUN rustup component add clippy

# pip based tool installs
RUN apt update \
    && apt install curl git python3-pip --no-install-recommends -y \
    && pip install lizard --break-system-packages \
    && pip install git+https://github.com/codespell-project/codespell.git --break-system-packages \
    && rm -rf /var/lib/apt/lists/*

# Copy cargo-deny binary from a previous CI step
# on amd64 systems, these are empty mock files that are deleted in the next step
COPY ./cargo-deny /usr/local/bin/cargo-deny

# Add Homebrew to PATH
ENV PATH="/home/linuxbrew/.linuxbrew/bin:${PATH}"

# Install cargo-udeps and cargo-deny using Homebrew on amd64
RUN ARCH=$(uname -m) && \
    if [ "$ARCH" = "x86_64" ]; then \
    rm /usr/local/cargo/bin/cargo-deny; \
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"; \
    brew install cargo-udeps; \
    brew install cargo-deny; \
    fi

# Install cargo-udeps from Github release
RUN ARCH=$(uname -m) && \
    if [ "$ARCH" = "aarch64" ]; then \
    export LATEST_UDEPS_VERSION=$(git ls-remote --tags --sort="v:refname" --refs https://github.com/est31/cargo-udeps.git | tail -n1 | awk '{print $2}' | awk -F '/' '{print $3}'); \
    curl -LJO https://github.com/est31/cargo-udeps/releases/download/${LATEST_UDEPS_VERSION}/cargo-udeps-${LATEST_UDEPS_VERSION}-aarch64-unknown-linux-gnu.tar.gz; \
    tar -xzf cargo-udeps-v0.1.42-aarch64-unknown-linux-gnu.tar.gz; \
    mv cargo-udeps-v0.1.42-aarch64-unknown-linux-gnu/cargo-udeps ./bin/; \
    fi

ADD analyze.sh analyze.sh
RUN chmod a+x analyze.sh

# limits after which lizard will raise a warning
ENV MAX_FUNCTION_LENGTH=200
ENV MAX_PARAMETER_COUNT=5
ENV MAX_COMPLEXITY=5

# codespell configuration
ENV CODESPELL_SKIP_FILES="*.txt,./target,./.cargo,./.git"
ENV CODESPELL_ALLOW_WORDS="crate"

# move target dir out of project folder
RUN mkdir /target
ENV CARGO_TARGET_DIR=/target

WORKDIR /project

CMD ["/analyze.sh"]
