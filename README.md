# Static Analysis for Rust Code

This container runs a suite of static analysis tools on your Rust code and exits with code 0 if all of the following tests pass:
- cargo-udeps: Checks for unused dependencies in your `Cargo.toml` file
- cargo-deny: Analyzes your dependencies for the overall quality and also ensures they match your license requirements
- Lizard: Computes the Cyclomatic Complexity and other metrics
- Codespell: Checks for common typos in your code

# How To

To run on the current crate:
```sh
docker run -v $(pwd):/project snowpoke93/rust-static-analysis
```

To initialize a `cargo-deny` config file:
```sh
docker run -v $(pwd):/project snowpoke93/rust-static-analysis cargo-deny init
```

# Customize

You can adjust some settings by setting the following environment variables:
- `MAX_FUNCTION_LENGTH`: Maximal allowed lines of code in a function. *(default: 200)*
- `MAX_PARAMETER_COUNT`: Number of maximal allowed parameters for a function. *(default: 5)*
- `MAX_COMPLEXITY`: Maximal allowed Cyclomatic Complexity for a function. *(default: 5)*
- `CODESPELL_SKIP_FILES`: Comma-separated list of files to be skipped by Codespell. *(default: "\*.txt,./target,./.cargo")*
- `CODESPELL_ALLOW_WORDS`: Comma-separated list of words that should not be recognized as typos by Codespell. *(default: "crate")*
