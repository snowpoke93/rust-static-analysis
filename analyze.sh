#!/bin/sh

# compute cyclomatic complexity
printf "\n-- Tool: lizard --\n"
lizard -Eduplicate --length $MAX_FUNCTION_LENGTH --CCN $MAX_COMPLEXITY -a $MAX_PARAMETER_COUNT
lizard_result=$?

# show unused dependencies
printf "\n\n-- Tool: cargo-udeps --\n"
cargo +nightly udeps --quiet
udeps_result=$?

# check quality of dependency crates
printf "\n\n-- Tool: cargo-deny --\n"
cargo deny --all-features check
deny_result=$?

# check for typos in code
printf "\n\n-- Tool: codespell --\n"
codespell --skip=$CODESPELL_SKIP_FILES -L $CODESPELL_ALLOW_WORDS
codespell_result=$?

printf "\n\n"

[ $lizard_result -eq 0 ] && echo "lizard: PASS" || echo "lizard: FAIL (return code $lizard_result)" 
[ $udeps_result -eq 0 ] && echo "cargo-udeps: PASS" || echo "cargo-udeps: FAIL (return code $udeps_result)" 
[ $deny_result -eq 0 ] && echo "cargo-deny: PASS" || echo "cargo-deny: FAIL (return code $deny_result)" 
[ $codespell_result -eq 0 ] && echo "codespell: PASS" || echo "codespell: FAIL (return code $codespell_result)" 

# if any of the analyzers returned an error, exit with error code 1
([ $lizard_result -eq 0 ] && [ $udeps_result -eq 0 ] && [ $deny_result -eq 0 ] && [ $codespell_result -eq 0 ]) || exit 1

